#include "Ubx_Gps.cpp"
#include <Arduino.h>

GPS gps;

//Centro da circulo da área
geoPoint center;

//Posição atual
geoPoint current;

void setup()
{
  Serial.begin(9600);
  Serial.setRxBufferSize(256);

  //Random set of coordinates
  center.lat = 23.00;
  center.lon = 5.00;

  gps.configGPS(2);
}

void loop()
{
  int msgType = gps.processGPS();
  if (msgType == MT_NAV_POSLLH)
  {
    Serial.print("iTOW: ");
    Serial.print(ubxMessage.navPosllh.iTOW);
    Serial.print(" lat/lon: ");
    Serial.print(ubxMessage.navPosllh.lat / 1000000.0f);
    Serial.print("/");
    Serial.println(ubxMessage.navPosllh.lon / 1000000.0f);
    current.lat = ubxMessage.navPosllh.lat;
    current.lon = ubxMessage.navPosllh.lon;
    Serial.print("Geofence status: ");
    gps.checkGeofence(center, current) ? Serial.println("Inside ") : Serial.println("Outside ");
  }
  else if (msgType == MT_NAV_STATUS)
  {
    Serial.print("gpsFix: ");
    Serial.print(ubxMessage.navStatus.gpsFix);
    Serial.print(" psmState: ");
    Serial.println(ubxMessage.navStatus.flags2, BIN);
  }
  else if (msgType == MT_NAV_SOL)
  {
    Serial.print("NumSVs: ");
    Serial.println(ubxMessage.navSol.numSV);
  }
}